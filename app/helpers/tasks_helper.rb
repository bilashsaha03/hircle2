module TasksHelper
  def task_activities(task)
    comments = []
    task.sub_tasks.each do |sub_task|
      sub_task.comments.each do |comment|
        comments << comment
      end
    end
    comments.sort!{|c1,c2| c2.created_at <=> c1.created_at}
    return comments
  end
end
