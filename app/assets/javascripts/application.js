// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require jquery-ui
//= require tag-it
//= require date-picker
//= require jquery.purr
//= require best_in_place
//= require_tree .

$(document).ready(function(){
    $('.best_in_place').best_in_place()



    $("a#new-task-btn").click(function(){
        var task_id = 0;
       var task = '';

       $.ajax({
           url: "/tasks",
           type: 'POST',
           dataType: 'JSON',
           data: {task: {title: "Enter New task"}},
           success: function(response){
             if(eval(response.length) > 0){
                 alert(response)
             }
             else
             {
                 task_id = eval(response).id;
                 task = '<div class="span4 task_'+task_id+'"><div class="task"><div class="task-title"><div class="task-title-text"><span data-type="input" data-attribute="title" data-object="task" data-url="/tasks/'+task_id+'" '+'id="best_in_place_task_'+task_id+'_title" class="best_in_place">Enter News Task</span></div><div class="task-options"><div class="btn-group"><a href="javascript: void(0)" data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></a><ul class="dropdown-menu"><li task_id="'+task_id+'" class="add-sub-task"><a href="javascript: void(0)"><i class="icon-plus-sign"></i>New Sub Task</a></li><li class="delete-task" task_id="'+task_id+'"><a href="javascript: void(0)"><i class="icon-trash"></i>Delete</a></li><li class="copy-task" task_id="'+task_id+'"><a href="javascript: void(0)"><i class="icon-share"></i>Copy</a></li></ul></div></div><div style="clear: both;"></div><div class="task-date"><input type="text" value="Jun 13" size="10" class="datepicker"></div></div><div class="row-fluid time_tracking"> <div class="span3"> Start Tracking <br> <i class="clock icon-play-circle"></i>00:00:00 </div> <div class="span3"> Today <br> 99:99h </div> <div class="span3"> Yesterday <br> 88:88h </div> <div class="span3"> total <br> 45:78h </div> </div><div class="sub-tasks"></div><div id="task_activities_'+task_id+'"><div class="task-activity"><div class="task-activity-text">0 activites</div><div class="task-activity-action">-</div></div></div></div></div>';
                 $(".task-row").append(task)
             }
           }
       })
    });


    $('.span9').delegate('li.add-sub-task', 'click', function() {
        addSubTask($(this))
    });
    $('.span9').delegate('li.delete-task', 'click', function() {
        deleteTask($(this))
    });
    $('.span9').delegate('li.copy-task', 'click', function() {
        copyTask($(this))
    });


    $(".task-row").delegate(".datepicker", "focusin", function(){
        $(this).datepicker({format: 'M dd'}).change(function(){
            var task_date = $(this);
            var task_id = task_date.parent().parent().find("ul li.add-sub-task").attr('task_id');

            $.ajax({
                url: "/tasks/" + task_id,
                type: 'PUT',
                dataType: 'JSON',
                data: {"task[start_date]" : task_date.val()},
                success: function(response){
                    //alert('---')
                }
            })
            $(this).datepicker('hide')
        })

    });

    $(".span2 .datepicker").datepicker({format: 'M dd'}).on("changeDate", function(ev){
        //alert(ev.date)
        window.location = "/?start_date=" + ev.date;
    })


    $('.span9').delegate('.sub-task', 'dblclick', function() {
      var sub_task = $(this);
      $("#hircle_modal").modal('show');
        var subTaskId = 0

        str = $(this).find("span.best_in_place").attr('id');
        var matches = str.match(/\d+/g);
        for (var index = 0; index < matches.length; ++index) {
            subTaskId = parseInt(matches[index], 10);
        }


      $.ajax({
          url: "/sub_tasks/" + subTaskId,
          type: 'GET',
          dataType: 'html',
          beforeSend: function(){
              $("#modal_loading").html('Loading...')
          },
          success: function(response){
             $("#hircle_modal").html(response)
             $('p#priorities span.best_in_place').bind("ajax:success", function(e,best_in_place_sub_task_response){
                  var sub_task_response = $.parseJSON(best_in_place_sub_task_response);
                  sub_task.parent().find("div.priority").css('color',sub_task_response.priority.color_code)
             });
          }
      })
    });



    $('#hircle_modal, #hircle_task_modal').delegate('textarea.comment_description', 'keyup', function(e) {
       var text_area = $(this);
        if(e.which == 13) {
            $.ajax({
                url: "/comments",
                type: 'POST',
                data: text_area.closest('form').serialize(),
                success: function(response){
                    text_area.parent().parent().parent().find(".sub_task_comments").html(response)
                    text_area.val('')
                }
            })
            e.preventDefault();
        }
    });

    $('#hircle_modal').on('hidden', function () {
        var task_id = $("#task_id_modal").val()
        $.ajax({
            url: "/comments",
            data: {id: task_id},
            type: 'GET',
            success: function(response){
              $("#task_activities_" + task_id).html(response)
            }

        })
    });

    /*$(".accordion" ).accordion({
        collapsible: true ,
        active: true,
        header: ".task-activity"
    });*/

    $(".accordion .task-activity").click(function(){
        var accordion_action = $(this).find(".task-activity-action")
        if(accordion_action.text() == '+'){
            $(this).next().show();
            accordion_action.text('-')
        }
        else{
            if(accordion_action.text() == '-'){
                $(this).next().hide();
                accordion_action.text('+')
            }
        }

    });

    $("li.status").click(function(){
       var status = $(this);
       window.location = "/?status_id=" + status.attr('status_id')
    });

    $('.span9').delegate('.task-title', 'dblclick', function() {
       var task_modal = $("#hircle_task_modal").modal('show');
       var task_id = $(this).find(".add-sub-task").attr('task_id');
       $.ajax({
           url: "/tasks/" + task_id,
           type: 'GET',
           success: function(response){
               task_modal.html(response);
               $(".people").droppable({
                   accept: '.member',
                   drop: function(event, ui) {
                       var sub_task_id = $(this).attr("sub_task_id")
                       var user_id = $(ui.draggable).attr('user_id')
                       var new_member = $(ui.draggable).clone();
                       var alreadyMemberAdded = false;
                       new_member.removeClass('member').addClass('new_member')

                       $(this).find(".new_member").each(function(index,item){
                           item = $(item)
                               if(item.attr('user_id') == user_id){
                                 alreadyMemberAdded = true;
                                 return ;
                               }
                       });
                       if(!alreadyMemberAdded){
                           $(this).append(new_member);
                           new_member.draggable();
                           $.ajax({
                               url: "/sub_tasks/"+sub_task_id+"/add_member?user_id=" + user_id,
                               type :'POST',
                               success: function(response){

                               }
                           })
                       }

                   }
               });
               $(".member").draggable({
                   helper: 'clone'
               });
               $(".task_modal_content").droppable({
                   accept: '.new_member',
                   drop: function(event,ui){
                       var user_id = $(ui.draggable).attr('user_id')
                       var sub_task_id = $(ui.draggable).parent().attr('sub_task_id')
                      $.ajax({
                          url: "/sub_tasks/"+sub_task_id+"/delete_member?user_id=" + user_id,
                          type: "DELETE",
                          success: function(response){
                              ui.draggable.remove();
                          }
                      })
                   }
               })
           }
       })

    });

    $("i.clock").click(function(){
      if($(this).hasClass('icon-play-circle')){
          $(this).removeClass('icon-play-circle')
          $(this).addClass('icon-stop')
      }
        else{
          $(this).removeClass('icon-stop')
          $(this).addClass('icon-play-circle')
      }
    })





})

function clock()
{
    var d=new Date();
    ap="am";
    h=d.getHours();
    m=d.getMinutes();
    s=d.getSeconds();
    if (h>11) { ap = "pm"; }
    if (h>12) { h = h-12; }
    if (h==0) { h = 12; }
    if (m<10) { m = "0" + m; }
    if (s<10) { s = "0" + s; }
    document.getElementById('timehere').innerHTML=h + ":" + m + ":" + s + " " + ap;
    t=setTimeout('mytime()',500);
}

function buildTask(task_id, response){
    var title = response.title;
    var task = '<div class="span4 task_'+task_id+'"><div class="task"><div class="task-title"><div class="task-title-text"><span data-type="input" data-attribute="title" data-object="task" data-url="/tasks/'+task_id+'" '+'id="best_in_place_task_'+task_id+'_title" class="best_in_place">'+title+'</span></div><div class="task-options"><div class="btn-group"><a href="javascript: void(0)" data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></a><ul class="dropdown-menu"><li task_id="'+task_id+'" class="add-sub-task"><a href="javascript: void(0)"><i class="icon-plus-sign"></i>New Sub Task</a></li><li class="delete-task" task_id="'+task_id+'"><a href="javascript: void(0)"><i class="icon-trash"></i>Delete</a></li><li class="copy-task" task_id="'+task_id+'"><a href="javascript: void(0)"><i class="icon-share"></i>Copy</a></li></ul></div></div><div style="clear: both;"></div><div class="task-date"><input type="text" value="Jun 13" size="10" class="datepicker"></div></div><div class="row-fluid time_tracking"> <div class="span3"> Start Tracking <br> <i class="clock icon-play-circle"></i>00:00:00 </div> <div class="span3"> Today <br> 99:99h </div> <div class="span3"> Yesterday <br> 88:88h </div> <div class="span3"> total <br> 45:78h </div> </div><div class="sub-tasks"></div><div id="task_activities_'+task_id+'"><div class="task-activity"><div class="task-activity-text">0 activites</div><div class="task-activity-action">-</div></div></div></div></div>';
    var sub_tasks = ""
    return task;
}

function buildSubTask(sub_task_id, sub_task_title, sub_task_color_code){
    var sub_task = '<div class="sub-task-panel"><div class="sub-task"><div class="priority" style="color: '+sub_task_color_code+'">◥</div><div class="sub-task-title"><span data-type="input" data-attribute="title" data-object="sub_task" data-url="/sub_tasks/'+sub_task_id+'" '+'id="best_in_place_task_'+sub_task_id+'_title" class="best_in_place">'+ sub_task_title+'</span></div><div class="sub-task-date">26 May</div></div></div>';
    return sub_task
}

function addSubTask(new_sub_task){
    var sub_task_id = 0;
    var sub_task = '';
    var task_id = new_sub_task.attr('task_id')

    $.ajax({
        url: "/sub_tasks",
        type: 'POST',
        dataType: 'JSON',
        data: {sub_task: {title: "Enter New Sub task", task_id: task_id}},
        success: function(response){
            if(eval(response.length) > 0){
                alert(response)
            }
            else
            {
                sub_task_id = eval(response).id;
                sub_task = '<div class="sub-task-panel"><div class="sub-task"><div class="priority" style="color: '+eval(response).priority.color_code+'">◥</div><div class="sub-task-title"><span data-type="input" data-attribute="title" data-object="sub_task" data-url="/sub_tasks/'+sub_task_id+'" '+'id="best_in_place_task_'+sub_task_id+'_title" class="best_in_place">Enter New Sub Task</span></div><div class="sub-task-date">26 May</div></div></div>';
                $(".task_" + task_id).find('.sub-tasks').append(sub_task)
            }
        }
    })
}

function deleteTask(task){
  if(confirm("Are You Sure?")){
      var taskId = task.attr('task_id')
      $.ajax({
          url: "/tasks/" + taskId,
          type: "DELETE",
          dataType: 'JSON',
          data: {id: taskId},
          success: function(){
              $(".task_" + taskId).remove()
          }
      })
  }
}

function copyTask(task){
  var taskId = task.attr('task_id')
  var task = $(".task_" + taskId)
  $.ajax({
      url: "tasks/new",
      type: 'GET',
      dataType: 'JSON',
      data: {id: taskId},
      success: function(response){
          var task = buildTask(response.id,response)
          task = $(task)
          var sub_tasks = ""
          $.each(response.sub_tasks, function(index, sub_task){
              sub_tasks += buildSubTask(sub_task.id,sub_task.title, sub_task.priority.color_code)
          })

          task.find('.sub-tasks').html(sub_tasks)
          $(".task-row").append(task)
      }

  })
}

