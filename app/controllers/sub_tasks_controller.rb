class SubTasksController < ApplicationController
  respond_to :json

  def show
    sub_task = SubTask.find(params[:id])

    respond_to do |format|
      format.html {
        render :partial => 'shared/sub_task_panel', :locals => {:sub_task => sub_task}
      }
      format.json{
        render :json => sub_task.to_json(:include => [:priority, :comments])
      }
    end
  end

  def create
    sub_task = SubTask.new(params[:sub_task])
    sub_task.description = "Enter Description"
    sub_task.priority = Priority.default_priority.first
    if sub_task.save
      render :json => sub_task.to_json(:include => [:priority, :comments])
    else
      render :json => sub_task.errors.full_messages.to_json
    end
  end
  def update
    sub_task = SubTask.find(params[:id])
    if sub_task.update_attributes(params[:sub_task])
      render :json => sub_task.to_json(:include => [:priority, :comments])
    else
      render :json => sub_task.errors.full_messages.to_json
    end
  end

  def add_member
    sub_task = SubTask.find(params[:sub_task_id])
    sub_task.users << User.find(params[:user_id])
    sub_task.save
    render :nothing => true
  end
  def delete_member
    sub_task_user_relation = SubTasksUser.find_by_user_id_and_sub_task_id(params[:user_id],params[:sub_task_id])
    sub_task_user_relation.destroy
    render :nothing => true
  end
end
