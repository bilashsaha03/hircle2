class CommentsController < ApplicationController
  include TasksHelper

  def index
    task = Task.find(params[:id])
    comments = task_activities(task)
    respond_to do |format|
      format.html {
        render :partial => 'home/sub_task_activities', :locals => {:activities => comments}
      }
      format.json{

      }
    end
  end

  def create
    comment = Comment.new(params[:comment])
    comment.user = current_user
    comment.save
    respond_to do |format|
      format.html {
        render :partial => 'shared/comments', :locals => {:comments => comment.sub_task.comments}
      }
      format.json{

      }
    end
  end
end
