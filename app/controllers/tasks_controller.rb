class TasksController < ApplicationController
  respond_to :json

  def show
    @task = Task.find(params[:id])
    render :layout => false
  end

  def create
    task = Task.new(params[:task])
    task.status_id = 1
    if task.save
      render :json => task.to_json
    else
      render :json => task.errors.full_messages.to_json
    end
  end
  def update
    task = Task.find(params[:id])
    if task.update_attributes(params[:task])
      render :json => task.to_json
    else
      render :json => task.errors.full_messages.to_json
    end
  end

  # Used for copying a task
  def new
    task = Task.find(params[:id])
    new_task = task.dup :include => :sub_tasks
    new_task.save
    new_task.sub_tasks.each do |sub_task|
      sub_task.priority = Priority.default_priority.first
      sub_task.save
    end

    render :json => new_task.to_json(:include => {:sub_tasks => {:include => :priority}})
  end

  def destroy
    task = Task.find(params[:id])
    task.destroy
    render :json => {:success => true}
  end


end
