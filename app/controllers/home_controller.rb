class HomeController < ApplicationController
  def index
    @tasks = Task.order("start_date DESC")
    if params[:start_date].present?
      @tasks = @tasks.where("date(start_date) = ?",Date.parse(params[:start_date]).strftime("%Y-%m-%d"))
    end

    if params[:tag_choices].present?
      tag_choices = params[:tag_choices].split(',')
      tags = ""
      tag_choices.each_with_index do |tag_choice,index|
        tags << " #{index > 0 ? 'OR' : ''} lower(description) like '%#{tag_choice.downcase}%' OR lower(title) like '%#{tag_choice.downcase}%'"
      end
      @tasks = @tasks.where(tags)
    end

    if params[:status_id].present?
      @tasks = @tasks.where("status_id=?", params[:status_id])
    end

    @tasks = @tasks.paginate :per_page => 6, :page => params[:page]
  end
end
