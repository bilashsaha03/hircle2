class Priority < ActiveRecord::Base
  attr_accessible :color_code, :description, :title
  scope :default_priority, where("is_default=?", true)
end
