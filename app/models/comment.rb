class Comment < ActiveRecord::Base
  attr_accessible :description, :sub_task_id, :user_id
  belongs_to :user
  belongs_to :sub_task
end
