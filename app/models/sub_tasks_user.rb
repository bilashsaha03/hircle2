class SubTasksUser < ActiveRecord::Base
  attr_accessible :sub_task_id, :user_id
  belongs_to :sub_task
  belongs_to :user
  validates_uniqueness_of :sub_task_id, :scope => :user_id

end
