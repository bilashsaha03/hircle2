class Task < ActiveRecord::Base
  attr_accessible :description, :end_date, :start_date, :title, :status_id
  has_many :sub_tasks, :dependent => :destroy

  belongs_to :status


  audited
end
