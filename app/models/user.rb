class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name
  # attr_accessible :title, :body

  has_many :comments, :dependent => :destroy
  has_many :sub_tasks, :through => :sub_tasks_users, :dependent => :destroy
  has_many :sub_tasks_users

  attr_accessible :avatar
  has_attached_file :avatar, :styles => { :thumb => "37x37>"}, :default_url => "/assets/missing_icon.jpg"

  def update_with_password(params={})
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation) if params[:password_confirmation].blank?
    end
    update_attributes(params)
  end


end
