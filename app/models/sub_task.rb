class SubTask < ActiveRecord::Base
  attr_accessible :description, :priority_id, :title, :task_id
  belongs_to :task
  belongs_to :priority

  has_many :comments, :dependent => :destroy
  has_many :users, :through => :sub_tasks_users, :dependent => :destroy
  has_many :sub_tasks_users


  audited :associated_with => :task
end
