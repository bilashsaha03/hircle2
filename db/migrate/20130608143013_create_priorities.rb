class CreatePriorities < ActiveRecord::Migration
  def change
    create_table :priorities do |t|
      t.string :title
      t.string :color_code
      t.text :description
      t.boolean :is_default, :default => false

      t.timestamps
    end
  end
end
