class CreateSubTaskUsers < ActiveRecord::Migration
  def change
    create_table :sub_tasks_users do |t|
      t.integer :sub_task_id
      t.integer :user_id

      t.timestamps
    end
  end
end
